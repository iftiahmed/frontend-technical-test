import React, { Component } from 'react';
import { getVehicleData } from '../api';

export default
class VehicleDetails extends Component {

	constructor(props) {
		super(props);

		this.state = {
			vehicle: null,
			media: null
		}
	}

	componentDidMount() {
		getVehicleData(this.props.vehicle.id, (vehicle) => {
			this.setState({
				vehicle
			})
		});
		this.setState({
			media: this.props.media
		});
	}

	render() {
		if (this.state.vehicle) {
			const vehicle = JSON.parse(this.state.vehicle);
			const media = this.state.media[0];
			return (
				<div className="vehicle-inner">
					<div className="vehicle-img">
						<img src={media.url} alt={media.name} />
					</div>
					<div className="vehicle-details">
						<h2 className="vehicle-details-name">{vehicle.id}</h2>
						<div className="vehicle-details-price">From {vehicle.price}</div>
						<div className="vehicle-details-description">{vehicle.description}</div>
					</div>
				</div>
			);
		}
		return (<h2>Loading...</h2>);
	}
}
