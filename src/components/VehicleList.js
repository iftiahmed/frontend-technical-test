import React, { Component } from 'react';
import { getData } from '../api';
import VehicleDetails from './VehicleDetails';

export default
class VehicleList extends Component {

	constructor(props) {
		super(props);

		this.state = {
			data: null
		}
	}

	componentDidMount() {
		getData((data) => {
			this.setState({
				data
			})
		});
	}

	render() {
		if(this.state.data) {
			const data = JSON.parse(this.state.data);

		    return (
		    	<ul className="vehicles">
		    	{
			    	data.vehicles.map((vehicle, index) => (
			    		<li key={index} className="vehicle" data-vehicle-id={vehicle.id}>
		    				<VehicleDetails vehicle={vehicle} media={vehicle.media} />
		    			</li>
			    	))
		    	}
			    </ul>
		    )
	    }

		return (<h1>Loading...</h1>);
	}
}
